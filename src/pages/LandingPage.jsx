import { useSearchParams } from "react-router-dom";
import { PeliculasGrid } from "../components/PeliculasGrid";
import { BarraBusqueda } from "../components/BarraBusqueda";
import { useDebounce } from "../hooks/useDebounce";

export function LandingPage() {
  const [query] = useSearchParams();
  const search = query.get("search");

  const debouncedSearch = useDebounce(search, 300);
  return (
    <div>
      <BarraBusqueda />
      <PeliculasGrid key={debouncedSearch} search={debouncedSearch} />
    </div>
  );
}