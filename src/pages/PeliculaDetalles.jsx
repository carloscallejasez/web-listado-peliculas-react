import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { get } from "../utils/httpClient";
import styles from "./PeliculaDetalles.module.css";

export function PeliculaDetalles() {
  const { movieId } = useParams();
  const [movie, setMovie] = useState(null);

  useEffect(() => {
    get("/movie/" + movieId).then((data) => {
      setMovie(data);
    });
  }, [movieId]);

  if (!movie) {
    return null;
  }

  const imageUrl = "https://image.tmdb.org/t/p/w500" + movie.poster_path;
  return (
    <div className={styles.contenedor}>
      <img
        className={styles.columna} 
        src={imageUrl}
        alt={movie.title}
      />
      <div className={styles.columna} >
        <p className={styles.firstItem}>
          <strong>TITULO:</strong> {movie.title}
        </p>
        <p>
          <strong>GÉNEROS:</strong>{" "}
          {movie.genres.map((genre) => genre.name).join(", ")}
        </p>
        <p>
          <strong>DESCRIPCIÓN:</strong> {movie.overview}
        </p>
      </div>
    </div>
  );
}