import styles from "./Empty.module.css";

export function Empty() {
  return <div className={styles.empty}>No hemos encontrado ningún resultado para la película buscada</div>;
}