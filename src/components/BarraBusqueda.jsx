import styles from "./BarraBusqueda.module.css";
import { FaSearch } from "react-icons/fa";
import { useSearchParams } from "react-router-dom";

export function BarraBusqueda() {
  const [query, setQuery] = useSearchParams();
  const search = query.get("search");

  const handleSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <form className={styles.contenedor} onSubmit={handleSubmit}>
      <div className={styles.caja}>
        <input
          className={styles.input}
          type="text"
          value={search ?? ""}
          autoFocus
          placeholder="Titulo"
          aria-label="Search Peliculas"
          onChange={(e) => {
            const value = e.target.value;

            setQuery({ search: value });
         
          }}
        />
        <FaSearch size={40} color="red"  className={styles.boton} />
      </div>
    </form>
  );
}