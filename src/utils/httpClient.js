const API = "https://api.themoviedb.org/3";

export function get(path) {
  return fetch(API + path, {
    headers: {
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI1MjdjZTMxMzY4ZGVhZDI1OTBhYzg4MGJhYjg0OTk4ZiIsInN1YiI6IjYyNWQyZjk3ZDM4YjU4MTdhZTI4YzI2MSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.v8CjhhFp-NRgsHK2xBvuqAQGPHw8zLeQsBAt__daAww",
      "Content-Type": "application/json;charset=utf-8",
    },
  }).then((result) => result.json());
}